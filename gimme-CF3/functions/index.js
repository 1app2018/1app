/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// [START all]
// [START import]
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
// Required for side-effects
const admin = require('firebase-admin');
admin.initializeApp();

const firestore = admin.firestore();
// [END import]

exports.MatchPlayers = functions.firestore.document('MatchPlayer/WaitingForGame')
  .onUpdate((change, context) => {
    //write logic here
    const newValue = change.after.data();

    if (!newValue || !newValue.PlayerUID || newValue.PlayerUID.size === 0) return true

    const player = newValue.PlayerUID[0]

    var PlayerRoom = firestore.collection("Rooms")

    return PlayerRoom.where("P2", "==", "none").get().then(querySnapshot => {
      if (querySnapshot.size === 0) {
        return PlayerRoom.add({
          P1: player,
          P2: "none"
        }).then(ref => {
          console.log('Added document with ID: ', ref.id);
        });
      }
      var roomId
      querySnapshot.forEach(doc => {
        roomId = doc.id
      })

      return PlayerRoom.doc(roomId).update({
        P2: player
      })
    }).then(() => {
      var WaitingPlayers = firestore.collection("MatchPlayer").doc("WaitingForGame")
      var fieldValue = firestore.FieldValue;

      return WaitingPlayers.update({
        PlayerUID: null
      });
    }
      ).catch(err => {
        console.log('Error getting documents', err);
      });
  });

exports.PrepareGame = functions.firestore.document('Rooms/{roomId}')
  .onWrite((change, context) => {

    if (!(change.after.data()) || change.after.data().P2 === "none") return true

    console.log(JSON.stringify(change.after.data()));
    var roomId = context.params.roomId

    console.log("roomId: " + roomId);
    // var WaitingPlayers = firestore.collection("MatchPlayer").doc("WaitingForGame")
    // var fieldValue = firestore.FieldValue;

    // var removeCapital = WaitingPlayers.update({
    //   PlayerUID: null
    // });

    //Add question 
    var Question = firestore.collection("Questions")
    var PlayerRoom = firestore.collection("Rooms").doc(roomId)
    var questionSnap = Question.limit(1);

    var qId = ""
    var questionStr = ""
    var answerStr = ""

    const newValue = change.after.data();
    const player1 = newValue.P1
    const player2 = newValue.P2
    console.log("P1: " + player1)
    console.log("P2: " + player2)
    var User1 = firestore.collection("Users").doc(player1)
    var User2 = firestore.collection("Users").doc(player2)

    return questionSnap.get()
      .then(snapshot => {
        let promise1;
        snapshot.forEach(doc => {
          console.log(doc.id, '=>', doc.data());
          // qId = doc.id
          // promise1 = Question.doc(doc.id).get().then(dataSnapshot => {
          //   let challenge = dataSnapshot.data();
          //   console.log("challenge: " + JSON.stringify(challenge));
          questionStr = doc.data().question;
          answerStr = doc.data().object_to_find;
          // });
        });
        return PlayerRoom.update({
          round: {
            question_id: qId,
            question: questionStr,
            object_to_find: answerStr
          }
        })
      }).then(ref => {
        console.log('Added question on room ID: ', roomId);
        return User1.update({ current_gamerm: roomId });
      }).then(() => {
        return User2.update({ current_gamerm: roomId });
      }).catch(err => console.error(err));

  })

// [END all]f
