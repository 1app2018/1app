//
//  GameplayEndRound.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/6/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import UIKit
import AVFoundation


protocol GameplayEndRoundDelegate: AnyObject {
    func didFinishRound(_ sender:AnyObject)
}

class GameplayEndRound: ViewController {

    var player: AVAudioPlayer?
    public var answer: String?
    public var question: String?
    public var round: String?
    public var winnerLoser: String?
    
    @IBOutlet var answerLbl: UILabel!
    @IBOutlet var questionsTextView: UITextView!
    
    @IBOutlet var winnerLoserLbl: UILabel!
    
    weak var delegate: GameplayEndRoundDelegate?
    var timer = Timer()
    var countDown = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        runTimer()
        // Do any additional setup after loading the view.
        answerLbl.text = answer!
        winnerLoserLbl.text = winnerLoser!
        questionsTextView.text = question!
        
        
        
        
        
//        roundLbl.text = round!
//
//        let label = winnerLoserLbl!
//        var bounds = label.bounds
//        label.font = label.font.withSize(100)
//        bounds.size = label.intrinsicContentSize
//        label.bounds = bounds
//        let scaleX = label.frame.size.width / bounds.size.width
//        let scaleY = label.frame.size.height / bounds.size.height
//        label.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
//        UIView.animate(withDuration: 1.0) {
//            self.winnerLoserLbl.transform = .identity
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        playSound()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func updateTimer () {
        countDown -= 1
        
        if countDown == 0 {
            timer.invalidate()
            delegate?.didFinishRound(self)
        }
        
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GameplayEndRound.updateTimer), userInfo: nil, repeats: true)
        
    }
    
    func playSound() {
        
        var Sound = "Loser"
        if winnerLoser! == "Winner" {
            Sound = "Winner"
        }
        
        guard let url = Bundle.main.url(forResource: Sound, withExtension: "m4a") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
