//
//  Profile.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class GimmePlayerProfileController: UIViewController{
    
    @IBOutlet weak var ProfilePicButton: UIButton!
    @IBOutlet weak var GUserNameField: UITextField!
    @IBOutlet weak var NickNameField: UITextField!
    @IBOutlet weak var SaveButton: UIButton!
    
    var handle : AuthStateDidChangeListenerHandle?
    var userProfile: User?
    
    override func viewDidLoad() {
        self.navigationController?.modalTransitionStyle = .crossDissolve
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // [START auth_listener]
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            // [START_EXCLUDE]
            // [END_EXCLUDE]
        }
        self.getUserDetails()
    }
    
    @IBAction func OnClickNickNameField(_ sender: Any) {
        NickNameField.resignFirstResponder()
    }
    
    @IBAction func OnClickSaveButton(_ sender: Any) {
        NickNameField.resignFirstResponder()
        self.saveUserDetails()
        performSegue(withIdentifier: "GoToHome", sender: self)
    }
    
    func getUserDetails() {
        userProfile = Auth.auth().currentUser
        print("GimmePlayerProfileController displayName: " + (userProfile?.displayName)!)
        GUserNameField.insertText((userProfile?.displayName)!)
        print("GimmePlayerProfileController email: " + (userProfile?.email)!)
        print("GimmePlayerProfileController picture: " + (userProfile?.photoURL?.absoluteString)!)
        let url = URL(string:(userProfile?.photoURL?.absoluteString)!)
        let data = try? Data(contentsOf: url!)
        let image: UIImage = UIImage(data: data!)!
        ProfilePicButton.setBackgroundImage(image, for: UIControlState.normal)
        print("GimmePlayerProfileController uid: " + (userProfile?.uid)!)
    }
    
    func saveUserDetails() {
//        performSegue(withIdentifier: "GoToHome", sender: self)
        UserDefaults.standard.setValue(NickNameField.text, forKey: "NickName")
        UserDefaults.standard.synchronize()
    }
}
