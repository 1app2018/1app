//
//  Home.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import AVFoundation

class HomeScreenController : UIViewController, InviteDelegate {

    @IBOutlet weak var PlayButton: UIButton!
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        playSound()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToHome(segue:UIStoryboardSegue) {
        
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "MainMenu", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
//            player.numberOfLoops = 20;
            player.volume = 0.6
            player.play()
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }

    
    @IBAction func OnClickPlay(_ sender: Any) {
        performSegue(withIdentifier: "PlayerMatch", sender: self)
    }
    
    @IBAction func inviteFriends(_ sender: UIButton) {
        
        if let invite = Invites.inviteDialog() {
            invite.setInviteDelegate(self)
            
            // NOTE: You must have the App Store ID set in your developer console project
            // in order for invitations to successfully be sent.
            
            // A message hint for the dialog. Note this manifests differently depending on the
            // received invitation type. For example, in an email invite this appears as the subject.
            invite.setMessage("Can you Beat me?!\n -\(GIDSignIn.sharedInstance().currentUser.profile.name!)")
            // Title for the dialog, this is what the user sees before sending the invites.
            invite.setTitle("Play with me :)")
            invite.setDeepLink("https://google.com")
            invite.setCallToActionText("Install!")
            invite.setCustomImage("https://cdn.iconscout.com/icon/premium/png-256-thumb/magnifier-562-615245.png")
            invite.open()
        }
    }
    
    func inviteFinished(withInvitations invitationIds: [String], error: Error?) {
        if let error = error {
            print("Failed: " + error.localizedDescription)
        } else {
            print("\(invitationIds.count) invites sent")
        }
    }
    
}
