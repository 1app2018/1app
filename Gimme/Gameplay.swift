//
//  Gameplay.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import AVFoundation
import NVActivityIndicatorView

struct Game {
    var answer: String
    var question: String
    var opponent: String
}

class Gameplay: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate, GameplayEndRoundDelegate {
    
    @IBOutlet var backBtn: UIButton!
    // MARK: - Properties
    lazy var vision = Vision.vision()
    
    var captureSession: AVCaptureSession = {
        let session = AVCaptureSession()
        session.sessionPreset = .high
        return session
    }()
    
    var round = 1
    var answer: String?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var photoOutput = AVCapturePhotoOutput()
    let output = AVCaptureVideoDataOutput()
    var userId : String! = Auth.auth().currentUser?.uid
    var roomId : String!
    var winnerLoser = ""
    let sampleBufferQueue = DispatchQueue.global(qos: .userInteractive)
    
    var cameraInput: AVCaptureDeviceInput? = nil
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var pointsLbl: UILabel!
    @IBOutlet var opponentNameLbl: UILabel!
    @IBOutlet var questionTextView: UITextView!
    @IBOutlet var captureBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        questionTextView.text = ""
        loadingIndicator.type = .pacman
        pointsLbl.text = ""
        
        let db = Firestore.firestore()
        db.collection("Rooms").document("dummy_room").addSnapshotListener { documentSnapshot, error in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            print("Current data: \(document)")
            
            guard let data = document.data() else {
                print("Document data was empty.")
                return
            }
            
            self.answer = document.get("round.object_to_find") as? String
            self.questionTextView.text = document.get("round.question") as? String
//            let opponent = document.get("player2.opponent") as? String
//            self.opponentNameLbl.text = "Playing Against \(opponent!)"
            
            
            if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                self.setupCaptureSession()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (authorized) in
                    DispatchQueue.main.async {
                        if authorized {
                            self.setupCaptureSession()
                        }
                    }
                })
            }
        }
        return
//        
//        
//        db.collection("Users").document(userId!).getDocument { (document, error) in
//            self.roomId = document?.get("current_gamerm") as? String
//            
//            db.collection("Rooms").document(self.roomId).addSnapshotListener { documentSnapshot, error in
//                guard let document = documentSnapshot else {
//                    print("Error fetching document: \(error!)")
//                    return
//                }
//                print("Current data: \(document)")
//                
//                guard let data = document.data() else {
//                    print("Document data was empty.")
//                    return
//                }
//                
//                self.answer = document.get("round.object_to_find") as? String
//                self.questionTextView.text = document.get("round.question") as? String
//                let opponent = document.get("player2.opponent") as? String
//                self.opponentNameLbl.text = "Playing Against \(opponent!)"
//                
//                
//                if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
//                    self.setupCaptureSession()
//                } else {
//                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (authorized) in
//                        DispatchQueue.main.async {
//                            if authorized {
//                                self.setupCaptureSession()
//                            }
//                        }
//                    })
//                }
//            }
//        }
        
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadingIndicator.isHidden = true;
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.portrait]
    }
    
    // MARK: - Camera Capture
    
    private func findCamera() -> AVCaptureDevice? {
        let deviceTypes: [AVCaptureDevice.DeviceType] = [
            .builtInDualCamera,
            .builtInTelephotoCamera,
            .builtInWideAngleCamera
        ]
        
        let discovery = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes,
                                                         mediaType: .video,
                                                         position: .back)
        
        return discovery.devices.first
    }
    
    private func setupCaptureSession() {
        guard captureSession.inputs.isEmpty else { return }
        guard let camera = findCamera() else {
            print("No camera found")
            return
        }
        
        do {
            self.cameraInput = try AVCaptureDeviceInput(device: camera)
            captureSession.addInput(cameraInput!)
            
            let preview = AVCaptureVideoPreviewLayer(session: captureSession)
            preview.frame = imageView.bounds
            preview.backgroundColor = UIColor.black.cgColor
            //            preview.videoGravity = .resizeAspect
            imageView.layer.addSublayer(preview)
            self.previewLayer = preview
            
            
            output.alwaysDiscardsLateVideoFrames = true
//            output.setSampleBufferDelegate(self, queue: sampleBufferQueue)
            
            
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
            if captureSession.canAddOutput(photoOutput) {
                captureSession.addOutput(photoOutput)
            }
            
            
            captureSession.addOutput(output)
            
            captureSession.startRunning()
            
        } catch let e {
            print("Error creating capture session: \(e)")
            return
        }
    }
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        
    }
    
    func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        captureSession.stopRunning()
        
        guard let rawImage = photo.cgImageRepresentation() else {
            print("Failed to get image")
            return
        }
        
        let cgImage = rawImage.takeRetainedValue()
        let orientation = photo.metadata[kCGImagePropertyOrientation as String] as! NSNumber
        let uiOrientation = UIImageOrientation(rawValue: orientation.intValue)!
        let image = UIImage(cgImage: cgImage, scale: 1, orientation: uiOrientation)
        imageView.image = image
        
        self.detectImage(self.imageView.image!)
        
    }
    
    
    @IBAction func capture(_ sender: UIButton) {
        
        if captureSession.isRunning {
            loadingIndicator.isHidden = false
            loadingIndicator.startAnimating()
            
            let settings = AVCapturePhotoSettings()
            photoOutput.capturePhoto(with: settings, delegate: self)
            captureBtn.isEnabled = false
        } else {
            captureSession.startRunning()
        }
        
    }
    
    func detectImage(_ imageParams: UIImage) {
        
//        imageView.image = imageParams
//        let options = VisionCloudDetectorOptions()
//        options.modelType = .latest
//        options.maxResults = 5
//
//        let labelDetector = vision.cloudLabelDetector()
//
//        let image = VisionImage(image: imageParams)
//
//        //        let image = VisionImage(image: imageView.image)
//
//        labelDetector.detect(in: image) { labels, error in
//            guard error == nil, let labels = labels, !labels.isEmpty else {
//                // ...
//                self.loadingIndicator.isHidden = true;
//                print(error.debugDescription)
//                return
//            }
//            self.loadingIndicator.isHidden = true;
//            // Labeled image
//            // START_EXCLUDE
//            print(labels)
//            //            for var label in labels {
//            //                print(label.label!)
//            //            }
//
//            //            let results = labels.map { label -> String in
//            //                "Label: \(String(describing: label.label ?? "")), " +
//            //                    "Confidence: \(label.confidence ?? 0), " +
//            //                "EntityID: \(label.entityId ?? "")"
//            //                }.joined(separator: "\n")
//
//            let results = labels.map { label -> String in
//                "\(String(describing: label.label ?? "")), "
//                }.joined(separator: "\n")
//
//            self.questionTextView.text = results
//            print(results)
////            self.answer = results
//
//            if results.lowercased().range(of: self.answer!.lowercased()) != nil {
//                self.performSegue(withIdentifier: "gameplayResult", sender: self)
//            } else {
//                let alert = UIAlertController(title: "", message: "Sorry", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//                self.setupCaptureSession()
//            }

            

        

        print("detectImage \(imageParams)")
//        let options = VisionCloudDetectorOptions()
//        options.modelType = .latest
//        options.maxResults = 5

        // [START config_label]
//        let options = VisionLabelDetectorOptions(
//            confidenceThreshold: 0.70
//        )
        // [END config_label]

        // [START init_label]
        let labelDetector = vision.cloudLabelDetector()
        // [END init_label]

        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = UIUtilities.visionImageOrientation(from: imageParams.imageOrientation)

        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: imageParams)
        visionImage.metadata = imageMetadata

        print("startinglabel detector")
        // [START detect_label]
        labelDetector.detect(in: visionImage) { labels, error in
            guard error == nil, let labels = labels, !labels.isEmpty else {
                // ...
                print("failed")
                self.loadingIndicator.isHidden = true;
                print(error.debugDescription)
                self.captureBtn.isEnabled = true
                return
            }
            print("detector completed")
            self.captureBtn.isEnabled = true
            self.loadingIndicator.isHidden = true;
            // Labeled image
            // START_EXCLUDE
//            print(labels)
//            for var label in labels {
//                print(label.label!)
//            }

//            let results = labels.map { label -> String in
//                "Label: \(String(describing: label.label ?? "")), " +
//                    "Confidence: \(label.confidence ?? 0), " +
//                "EntityID: \(label.entityId ?? "")"
//                }.joined(separator: "\n")

            let results = labels.map { label -> String in
                "\(String(describing: label.label ?? "")), "
                }.joined(separator: "\n")

//            self.questionTextView.text = results
            print(results)
            self.answer = "Computer"

            if results.lowercased().range(of: self.answer!.lowercased()) != nil {
                self.winnerLoser = "Winner"
                self.performSegue(withIdentifier: "gameplayResult", sender: self)
            } else {
               
                
//                let alert = UIAlertController(title: "You lose", message: "Sorry", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: {
//                     self.performSegue(withIdentifier: "unwindToHome", sender: self)
//                })

//                self.setupCaptureSession()
                self.winnerLoser = "You Lose"
                self.performSegue(withIdentifier: "gameplayResult", sender: self)
            }





//            self.perform(NSSelectorFromString("didFinishRound"), with: self, afterDelay: 3)
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
//                // your function here
//                self.didFinishRound(self)
//            }
        
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: GameplayEndRound.self) {
            let vc = segue.destination as! GameplayEndRound
            vc.answer = self.answer!
            vc.round = "Round \(round)"
            vc.question = questionTextView.text
            vc.winnerLoser = winnerLoser
            vc.delegate = self
        }
        
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        captureSession.removeInput(cameraInput!)
//        captureSession.removeOutput(photoOutput)
//        captureSession.removeOutput(output)
//
//    }
    
    
    func didFinishRound(_ sender: AnyObject) {
        
//        imageView.layer.removeFromSuperlayer()
//        imageView.image = nil
//        captureSession.startRunning()
//        captureSession.removeInput(cameraInput!)
//        captureSession.removeOutput(photoOutput)
//        captureSession.removeOutput(output)
//
//        imageView.image = nil
//        self.setupCaptureSession()
        self.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "showHome", sender: nil)
//            DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "unwindToHome", sender: self)
//                self.captureSession.removeInput(self.cameraInput!)
//                self.captureSession.removeOutput(self.photoOutput)
//                self.captureSession.removeOutput(self.output)
//
//                self.imageView.image = nil
////            self.backBtn.sendActions(for: .touchUpInside)
//            }
            
        })
//        self.dismiss(animated: false, completion: nil)
        
        
        return
        
        
        
        if round == 3 {
            performSegue(withIdentifier: "unwindToHome", sender: self)
            return
        }
            
        
        loadingIndicator.isHidden = true;
        questionTextView.text = "";
        captureBtn.isEnabled = true;
        
        captureSession.startRunning()
//        setupCaptureSession()
        
        round += 1
//        "Label: \(String(describing: label.label ?? "")), " +
//        roundLbl.text = "Round \(round)"
    }
    
}
