//
//  LeaderboardController.swift
//  Gimme
//
//  Created by Ehman Rioveros on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//
import Firebase
import UIKit

// Custom Cell
class PlayerTableViewCell: UITableViewCell {
        
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var rankLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
}

// Struct
struct Player {
    var username: String
    var img_url: String
}

class Leaderboard: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        loadFirestoreData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return players.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    var players = [Player]() // To be populated on viewDidLoad
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath)
            as! PlayerTableViewCell
        
        let player = players[indexPath.row]
        cell.nameLbl?.text = player.username
        cell.rankLbl?.text = String(indexPath.row + 1)
        let img_url = URL(string: player.img_url)!
        cell.profileImg?.sd_setImage(with: img_url, completed: nil)
        
        return cell
    }
    
    private func loadFirestoreData() {
        
        let db = Firestore.firestore()
        //.order(by: "win_rate", descending: true)
        
        db.collection("Users").order(by: "win_rate", descending: true).getDocuments()
            { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        // print("\(document.documentID) => \(document.data())")
                        let player = Player(username: document.get("user_name") as! String,
                                            img_url: document.get("img_url") as! String)
                        self.players.append(player)
                    }
                    self.tableView.reloadData()
                }
        }

    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

