//
//  Login.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import Foundation
import Firebase
import FirebaseUI
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate, FUIAuthDelegate {
    
    var auth:Auth?
    var authUI: FUIAuth? //only set internally but get externally
    var authStateListenerHandle: AuthStateDidChangeListenerHandle?
    
    let providers: [FUIAuthProvider] = [
        FUIGoogleAuth()
        ]
    
    override func viewDidLoad() {
        self.modalTransitionStyle = .crossDissolve
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.auth = Auth.auth()
        self.authUI = FUIAuth.defaultAuthUI()
        self.authUI?.delegate = self
        self.authUI?.providers = providers

    }
    
    @IBAction func loginAction(sender: AnyObject) {
        // Present the default login view controller provided by authUI
        let authViewController = authUI?.authViewController();
        self.present(authViewController!, animated: true, completion: nil)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        guard let authError = error else {
            print("LoginViewController displayName: " + (user?.displayName)!)
            print("LoginViewController email: " + (user?.email)!)
            print("LoginViewController picture: " + (user?.photoURL?.absoluteString)!)
            print("LoginViewController uid: " + (user?.uid)!)
            performSegue(withIdentifier: "GoToProfile", sender: self)
            return
        }
        
        let errorCode = UInt((authError as NSError).code)
        
        switch errorCode {
        case FUIAuthErrorCode.userCancelledSignIn.rawValue:
            print("User cancelled sign-in");
            break
            
        default:
            let detailedError = (authError as NSError).userInfo[NSUnderlyingErrorKey] ?? authError
            print("Login error: \((detailedError as! NSError).localizedDescription)");
        }
    }
    
    
}
