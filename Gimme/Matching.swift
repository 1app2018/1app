//
//  Matching.swift
//  Gimme
//
//  Created by Alexis Apuli on 9/5/18.
//  Copyright © 2018 Alexis Apuli. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseFirestore

class MatchGamePlayerController : UIViewController {
    
    var userId : String! = Auth.auth().currentUser?.uid
    let db = Firestore.firestore()
    //lazy var functions = Functions.functions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("MatchGamePlayerController viewDidLoad")
        self.AddtoWaitQueue()
        self.startGame()
        
    }
    
    func AddtoWaitQueue() {
        userId = Auth.auth().currentUser?.uid
        print("AddtoWaitQueue userId:" + userId)
        db.collection("MatchPlayer").document("WaitingForGame").updateData([
            "PlayerUID": FieldValue.arrayUnion([userId])
            ])
    }
    
    func startGame(){
        var startGame : Bool = false
        
        var listenP1 = db.collection("Rooms").whereField("P1", isEqualTo: userId)
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                startGame = true
        }

        listenP1.remove()
        
        var listenP2 = db.collection("Rooms").whereField("P2", isEqualTo: userId)
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                startGame = true
        }
        
        listenP2.remove()
        
        if startGame == true {
            performSegue(withIdentifier: "matchFound", sender: self)
        }
        
    }
    
}
